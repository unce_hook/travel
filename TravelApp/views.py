from django.shortcuts import get_object_or_404, render

# Create your views here.
from .models import User, Location, Visit

import time
from datetime import datetime, timedelta, timezone
from django.http import JsonResponse
from django.http import HttpResponseNotFound
from django.http import HttpResponseBadRequest
from django.core.exceptions import ValidationError


from django.views.decorators.csrf import csrf_exempt

import json


@csrf_exempt
def get_user(request, user_id):
    user = get_object_or_404(User, pk=user_id)

    if request.method == 'GET':

        user_dict = {}

        user_dict['id'] = user.id
        user_dict['email'] = user.email
        user_dict['first_name'] = user.first_name
        user_dict['last_name'] = user.last_name
        user_dict['gender'] = user.gender

        d = user.birth_date

        unix_time = int(time.mktime(d.timetuple()))

        user_dict['birth_date'] = unix_time

        response = JsonResponse(user_dict)

        return response

    else:

        try:
            data = json.loads(request.body.decode())
        except ValueError:
            return HttpResponseNotFound()

        user.email = data['email'] if data.get('email') != None else user.email
        user.first_name = data['first_name'] if data.get('first_name') != None else user.first_name
        user.last_name = data['last_name'] if data.get('last_name') != None else user.last_name
        user.gender = data['gender'] if data.get('gender') != None else user.gender

        if data.get('birth_date') != None:
            date = data.get('birth_date')
            epoch = datetime(1970, 1, 1, tzinfo=timezone.utc)
            date = date * timedelta(seconds=1) + epoch
            user.birth_date = date

        try:
            user.full_clean()
        except ValidationError as e:
            return HttpResponseBadRequest()

        user.save()

        return JsonResponse({})


@csrf_exempt
def get_locations(request, location_id):
    location = get_object_or_404(Location, pk=location_id)

    if request.method == 'GET':

        location_dict = {}

        location_dict['id'] = location.id
        location_dict['place'] = location.place
        location_dict['country'] = location.country
        location_dict['city'] = location.city
        location_dict['distance'] = location.distance

        response = JsonResponse(location_dict)

        return response

    else:

        try:
            data = json.loads(request.body.decode())
        except ValueError:
            return HttpResponseBadRequest()

        location.place = data['place'] if data.get('place') != None else location.place
        location.country = data['country'] if data.get('country') != None else location.country
        location.city = data['city'] if data.get('city') != None else location.city
        location.distance = data['distance'] if data.get('distance') != None else location.distance

        try:
            location.full_clean()
        except ValidationError as e:
            return HttpResponseBadRequest()

        location.save()

        return JsonResponse({})


@csrf_exempt
def get_visits(request, visit_id):
    visit = get_object_or_404(Visit, pk=visit_id)

    if request.method == 'GET':

        visit_dict = {}

        visit_dict['id'] = visit.id
        visit_dict['location'] = int(visit.location.__str__())
        visit_dict['user'] = int(visit.user.__str__())

        d = visit.visited_at
        unix_time = int(time.mktime(d.timetuple()))

        visit_dict['visited_at'] = unix_time

        visit_dict['mark'] = visit.mark

        response = JsonResponse(visit_dict)

        return response

    else:

        try:
            data = json.loads(request.body.decode())
        except ValueError:
            return HttpResponseBadRequest()

        if data.get('location') != None:

            try:
                loc = Location.objects.get(id=int(data['location']))
            except Location.DoesNotExist:
                return HttpResponseBadRequest()
            visit.location = loc

        if data.get('user') != None:

            try:
                us = User.objects.get(id=int(data['user']))
            except Location.DoesNotExist:
                return HttpResponseBadRequest()
            visit.user = us

        visit.mark = data['mark'] if data.get('mark') != None else visit.mark

        if data.get('visited_at') != None:
            date = data.get('visited_at')
            epoch = datetime(1970, 1, 1, tzinfo=timezone.utc)
            date = date * timedelta(seconds=1) + epoch

            visit.visited_at = date

        try:
            visit.full_clean()
        except ValidationError as e:
            return HttpResponseBadRequest()

        visit.save()

        return JsonResponse({})

@csrf_exempt
def get_user_visits(request, user_id):

    #object Q
    user = get_object_or_404(User, pk=user_id)

    visit = Visit.objects.filter(user=user_id)

    result = {}

    result['visits'] = []

    if 'fromDate' in request.GET:
        epoch = datetime(1970, 1, 1, tzinfo=timezone.utc)
        date = int(request.GET['fromDate']) * timedelta(seconds=1) + epoch
        visit = visit.filter(visited_at__gt=date)

    if 'toDate' in request.GET:
        epoch = datetime(1970, 1, 1, tzinfo=timezone.utc)
        date = int(request.GET['toDate']) * timedelta(seconds=1) + epoch
        visit =  visit.filter(visited_at__lt=date)

    if 'country' in request.GET:
        visit = visit.filter(location__country=request.GET['country'])


    if 'toDistance' in request.GET:
        visit = visit.filter(location__distance__lt=int(request.GET['toDistance']))

    visit.order_by('visited_at')

    for e in visit:
        dict = {}
        d = e.visited_at
        unix_time = int(time.mktime(d.timetuple()))
        dict['mark'] = e.mark
        dict['visited_at'] = unix_time
        dict['place'] = e.location.place

        result['visits'].append(dict)

    response = JsonResponse(result)

    return response

def get_location_avg(request, location_id):

    location = get_object_or_404(Location, pk=location_id)

    visits = Visit.objects.filter(location_id = location_id)

    if 'fromDate' in request.GET:
        epoch = datetime(1970, 1, 1, tzinfo=timezone.utc)
        date = int(request.GET['fromDate']) * timedelta(seconds=1) + epoch
        visits = visits.filter(visited_at__gt=date)

    if 'toDate' in request.GET:
        epoch = datetime(1970, 1, 1, tzinfo=timezone.utc)
        date = int(request.GET['toDate']) * timedelta(seconds=1) + epoch
        visits =  visits.filter(visited_at__lt=date)

    if 'fromAge' in request.GET:
        epoch = datetime(1970, 1, 1, tzinfo=timezone.utc)
        n = time.time()
        n = int(n) - int(request.GET['fromAge'])
        date = int(n) * timedelta(seconds=1) + epoch
        visits = visits.filter(user__birth_date__lt=date)

    if 'toAge' in request.GET:
        epoch = datetime(1970, 1, 1, tzinfo=timezone.utc)
        n = time.time()
        n = int(n) - int(request.GET['toAge'])
        date = int(n) * timedelta(seconds=1) + epoch
        visits = visits.filter(user__birth_date__gt=date)

    if 'gender' in request.GET:
        visits = visits.filter(user__gender=request.GET['gender'])

    c = 0
    sr = 0

    for e in visits:
        sr += e.mark
        c += 1

    sr = sr / c if c != 0 else 0.0

    result = {}

    result['avg'] = round(sr, 5)

    response = JsonResponse(result)

    return response


#n - b < get


@csrf_exempt
def new_user(request):
    try:
        data = json.loads(request.body.decode())
    except ValueError:
        return HttpResponseBadRequest()

    if data.get('email') == None:
        return HttpResponseBadRequest()
    if data.get('first_name') == None:
        return HttpResponseBadRequest()
    if data.get('last_name') == None:
        return HttpResponseBadRequest()
    if data.get('gender') == None:
        return HttpResponseBadRequest()

    if data.get('birth_date') != None:
        date = data.get('birth_date')
        epoch = datetime(1970, 1, 1, tzinfo=timezone.utc)
        date = date * timedelta(seconds=1) + epoch
    else:
        return HttpResponseBadRequest()


    user = User(id = data['id'],
                email = data['email'],
                first_name = data['first_name'],
                last_name =  data['last_name'],
                gender = data['gender'],
                birth_date = date
                )

    try:
        user.full_clean()
    except ValidationError as e:
        return HttpResponseBadRequest()

    user.save()

    return JsonResponse({})

@csrf_exempt
def new_location(request):
    try:
        data = json.loads(request.body.decode())
    except ValueError:
        return HttpResponseBadRequest()

    if data.get('place') == None:
        return HttpResponseBadRequest()
    if data.get('country') == None:
        return HttpResponseBadRequest()
    if data.get('city') == None:
        return HttpResponseBadRequest()
    if data.get('distance') == None:
        return HttpResponseBadRequest()


    location = Location(id = data['id'],
                place = data['place'],
                country = data['country'],
                city =  data['city'],
                distance = data['distance']
                )

    try:
        location.full_clean()
    except ValidationError as e:
        return HttpResponseBadRequest()

    location.save()

    return JsonResponse({})

@csrf_exempt
def new_visit(request):
    try:
        data = json.loads(request.body.decode())
    except ValueError:
        return HttpResponseBadRequest()

    if data.get('location') != None:

        try:
            loc = Location.objects.get(id=int(data['location']))
        except Location.DoesNotExist:
            return HttpResponseBadRequest()
    else:
        return HttpResponseBadRequest()

    if data.get('user') != None:

        try:
            us = User.objects.get(id=int(data['user']))
        except Location.DoesNotExist:
            return HttpResponseBadRequest()
    else:
        return HttpResponseBadRequest()


    if data.get('mark') == None:
        return HttpResponseBadRequest()

    if data.get('visited_at') != None:
        date = data.get('visited_at')
        epoch = datetime(1970, 1, 1, tzinfo=timezone.utc)
        date = date * timedelta(seconds=1) + epoch
    else:
        return HttpResponseBadRequest()


    visit = Visit(id = data['id'],
                location = loc,
                user = us,
                visited_at =  date,
                mark = data['mark']
                )

    try:
        visit.full_clean()
    except ValidationError as e:
        return HttpResponseBadRequest()

    visit.save()

    return JsonResponse({})