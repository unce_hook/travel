from django.contrib import admin

# Register your models here.


from .models import User, Visit, Location

admin.site.register(User)
admin.site.register(Visit)
admin.site.register(Location)