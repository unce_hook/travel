"""Travel URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from TravelApp import views as trav_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^users/(?P<user_id>[0-9]+)$',trav_views.get_user),
    url(r'^locations/(?P<location_id>[0-9]+)$',trav_views.get_locations),
    url(r'^visits/(?P<visit_id>[0-9]+)',trav_views.get_visits),
    url(r'^users/(?P<user_id>[0-9]+)/visits', trav_views.get_user_visits),
    url(r'^users/new', trav_views.new_user),
    url(r'^locations/new', trav_views.new_location),
    url(r'^visits/new', trav_views.new_visit),
    url(r'^locations/(?P<location_id>[0-9]+)/avg', trav_views.get_location_avg),

]
